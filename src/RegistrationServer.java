

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class RegistrationServer extends Thread {
    private static int port = 5000;
    public List <InetAddress> ips;
    public Map <Integer,InetAddress> ipmap;
    private static Thread thrd;
    public boolean flag = false;
    public int key = -1;
    public static RegistrationServer regServer;
    public static HeartBeat hb;


    public RegistrationServer(){
        ipmap = Collections.synchronizedMap(new TreeMap<Integer, InetAddress>());
        hb = new HeartBeat(this);
        thrd = new Thread(this);
        thrd.start();
    }
    private class Handler extends Thread{
        private Socket socket;
        private RegistrationServer rgs;
        private ObjectInputStream in;
        private ObjectOutputStream out;
        public Handler(Socket skt, RegistrationServer r){
            socket = skt;
            try {
                in = new ObjectInputStream((socket.getInputStream()));
                out = new ObjectOutputStream(socket.getOutputStream());
                rgs = r;
                rgs.addIP((Integer) in.readObject(),socket.getInetAddress());
                System.out.println(socket.getInetAddress());
            }
            catch(Exception e){
                e.printStackTrace();
            }

        }
        public void run() {

        }
    }

    public static void main(String[] args) throws Exception {

        System.out.println("The server is running.");
        ServerSocket listener = new ServerSocket(port);
        regServer = new RegistrationServer();
        try {
            while (true) {
                regServer.new Handler(listener.accept(),regServer).start();
            }
        } finally {
            listener.close();
        }
    }

    public void run(){
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(flag){
                Set s = ipmap.entrySet();
                synchronized (ipmap){
                    Iterator i = s.iterator();
                    while(i.hasNext()){
                        Map.Entry me = (Map.Entry)i.next();
                        InetAddress ina = (InetAddress) me.getValue();
                        try {
                            Socket chk = new Socket(ina.getHostAddress(), 7000);
                            ObjectOutputStream oos = new ObjectOutputStream(chk.getOutputStream());
                            oos.writeObject("Update");
                            oos.writeObject(ipmap);
                        }
                        catch(IOException e){
                        }
                    }
                    flag = false;
                }
            }
        }
    }

    public void addIP(int id, InetAddress i){
        synchronized (ipmap) {
            ipmap.put(id,i);
        }
        hb.addIP(id, i);
        flag = true;
    }

    public void removeIP(int id){
        synchronized (ipmap) {
            ipmap.remove(id);
        }
        flag = true;
    }

}
