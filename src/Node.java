
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class Node extends Thread {
    private static Thread thrd;
    private static ObjectOutputStream out;
    private static ObjectInputStream in;
    private static Hashtable<Integer,FingerTableValues> fingerTable;
    private static int id;
    public static String nodeip;
    private static FingerTableValues [] ftblval;
    public List<Integer> nodes,data;
    public NodeBeat nb;

    public Node(){
        thrd = new Thread(this);
        fingerTable = new Hashtable<Integer, FingerTableValues>();
        ftblval = new FingerTableValues[4];
        nodes = new ArrayList<Integer>();
        nb = new NodeBeat();
        data = new ArrayList<Integer>();
        thrd.start();
    }

    public static void main(String[] args) {
        try {

            Scanner scan = new Scanner(System.in);
            id = Integer.parseInt(args[1]);
            Node node = new Node();
            Socket client = new Socket(args[0],5000);
            out = new ObjectOutputStream(client.getOutputStream());
            in = new ObjectInputStream(client.getInputStream());
            nodeip = client.getLocalAddress().getHostAddress();
            out.writeObject(id);
            node.populateTable();
            client.close();
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                public void run() {
                    try {Thread.sleep(1000);
                        Socket send = new Socket(fingerTable.get(0).availableIP, 7000);
                        ObjectOutputStream os = new ObjectOutputStream(send.getOutputStream());
                        os.writeObject("Distribute");
                        os.writeObject(node.data);
                        os.close();
                        send.close();
                    }catch(Exception e){
                        System.out.println("No node alive");
                    }
                }
            }, "Shutdown-thread"));
            while (true){
                System.out.println("Enter 1 to store\n" + "Enter 2 to lookup\n" + "Enter 3 to display finger table\n" +"Enter 4 to display data\n"
                    +"Enter 5 to display node details");
                switch(Integer.parseInt(scan.nextLine())) {
                    case 1:
                        //int input = scan.nextInt();
                        String inputstr = scan.nextLine();
                        int input = (int)(node.hashFunction(inputstr)%16);
                        int t1 = node.findHost(input);
                        if(t1 == -1){
                            node.sendKey(ftblval[0].availableIP,input,id,"Key");
                            break;
                        }
                        node.sendKey(ftblval[t1].availableIP,input,id,"Key");
                        break;
                    case 2:
                        String findStr = scan.nextLine();
                        int find = (int)(node.hashFunction(findStr)%16);
                        if(node.data.contains(find)){
                            System.out.println(find);
                            break;
                        }
                        int t2 = node.findHost(find);
                        if(t2 == -1){
                            if(node.data.contains(find)){
                                System.out.println(find);
                                break;
                            }
                            node.sendKey(ftblval[0].availableIP,find,id,"Find");
                            break;
                        }
                        node.sendKey(ftblval[t2].availableIP,find,id,"Find");
                        break;
                    case 3:
                        node.displayTable();
                        break;
                    case 4:
                        node.displayData();
                        break;

                        case 5:
                            System.out.println(id + " " + nodeip);
                            break;

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void populateTable(){
        for(int j = 0; j < 4; j++){
            ftblval[j] = new FingerTableValues((id+(int)Math.pow(2,j))%16);
            nodes.add(ftblval[j].nodeid);
            fingerTable.put(j, ftblval[j]);
        }
    }

    public void displayTable(){
        for(int i = 0; i < 4; i++){
            FingerTableValues temp = fingerTable.get(i);
            System.out.println(i+" "+temp.nodeid+" "+temp.availableIP +" "+temp.successor);
        }
    }

    public void displayData(){
        for(int i = 0; i < data.size(); i++){
            System.out.print(data.get(i));
        }
        System.out.println();
    }
    public void sendKey(String h, int k,int s, String msg){
        try {
            System.out.println("Sending: " + k + " " + h);
            Socket send = new Socket(h, 7000);
            ObjectOutputStream os = new ObjectOutputStream(send.getOutputStream());
            os.writeObject(msg);
            os.writeObject(k);
            os.writeObject(s);
            os.close();
            send.close();
        }
        catch (Exception e){

        }
    }

    public int findHost(int in){
        FingerTableValues dest = null,temp;
        String dh = "";
        int k = -1;
        for(int i = 0; i < 4; i++){
            temp = fingerTable.get(i);
            if(temp.nodeid < in){
                if(dest != null ){
                    if(dest.nodeid < temp.nodeid && !temp.availableIP.equals(nodeip)) {
                        dest = temp;
                        k = i;
                    }
                }
                else{
                    if(!temp.availableIP.equals(nodeip)) {
                        dest = temp;
                        k = i;
                    }
                }
            }
            else if(in == temp.nodeid && !temp.availableIP.equals(nodeip)){
                k = i;
                break;
            }
            else
                continue;
        }
        return k;
    }

    public void updateTable(Map<Integer, InetAddress> m){
        for (int i = 0; i < 4; i++) {
            if(m.containsKey(nodes.get(i))) {
                //ftblval[i].setIP(m.get(nodes.get(i)).getHostAddress(),nodes.get(i));
                fingerTable.get(i).setIP(m.get(nodes.get(i)).getHostAddress(),nodes.get(i));
            }
            else{
                for(int j = nodes.get(i)+1;;j++) {
                    if(j == 16){
                        j = j%16;
                    }
                    if(m.containsKey(j)) {
                        //ftblval[i].setIP(m.get(j).getHostAddress(),j);
                        fingerTable.get(i).setIP(m.get(j).getHostAddress(),j);
                        break;
                    }
                }
            }
        }
    }

    public void storeData(int d){
        System.out.println("Storing "+d);
        data.add(d);
    }
    public boolean checkSuccessor(int n){
        for(int i = 1; i<=8 ; i++ ){
            if((n+i)%16 == id)
                return true;
        }
        return false;
    }

    public long hashFunction(String s){
        long h = 0;
        for(int i = 0; i < s.length(); i++){
            h = h + (long)Math.pow(3,i)*s.charAt(i);
        }
        return h;
    }

    public void run(){
        try {

            ServerSocket skt = new ServerSocket(7000);
            while (true) {
                Socket cskt = skt.accept();
                ObjectInputStream ois = new ObjectInputStream(cskt.getInputStream());
                String s = (String) ois.readObject();
                switch (s) {
                    case "Update":
                        updateTable((Map<Integer, InetAddress>) ois.readObject());
                        for(int i = 0; i < data.size();i++){
                            int j = data.get(i);
                            if(j!=id){
                                int tu = findHost(j);
                                if(tu == -1){
                                    //storeData(c);
                                    break;
                                }
                                sendKey(ftblval[tu].availableIP,j,id,"Key");
                                data.remove(i--);
                                displayData();
                            }
                        }
                        break;
                    case "Key":
                        int c = (Integer)ois.readObject();
                        int o = (Integer)ois.readObject();
                        if(id == o|| id == c || checkSuccessor(c)){
                            storeData(c);
                            break;
                        }
                        int tk = findHost(c);
                        if(tk == -1){
                            storeData(c);
                            break;
                        }
                        sendKey(ftblval[tk].availableIP,c,o,"Key");
                        break;
                    case "Find":
                        int f = (Integer)ois.readObject();
                        int n = (Integer)ois.readObject();
                        if(id == f || checkSuccessor(f)){
                            int tf = findHost(n);
                            if(data.contains(f)){
                                sendKey(ftblval[tf].availableIP,n,id,Integer.toString(f));
                            }
                            else{
                                sendKey(ftblval[tf].availableIP,n,id,"Not Found");
                            }
                            break;
                        }
                        int tn = findHost(f);
                        sendKey(ftblval[tn].availableIP,f,n,"Find");
                        break;

                    case "Distribute":
                        data.addAll((List<Integer>)ois.readObject());
                        displayData();
                        break;

                        default:
                            int i = (Integer)ois.readObject();
                            int r = (Integer)ois.readObject();
                            if(id == i){
                                System.out.println(s);
                            }
                            else{
                                System.out.println("Passing to " + i);
                                int td = findHost(i);
                                sendKey(ftblval[td].availableIP,i,r,s);
                            }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
