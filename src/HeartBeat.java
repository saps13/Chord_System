import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;

public class HeartBeat extends Thread{
    public Map<Integer, InetAddress> ipmap;
    public Thread thrd;
    public RegistrationServer regs;
    public HeartBeat(RegistrationServer r){
        ipmap = Collections.synchronizedMap(new TreeMap<Integer, InetAddress>());
        regs = r;
        thrd = new Thread(this);
        thrd.start();
    }
    public void run(){
        while(true){
            Set s = ipmap.entrySet();
            synchronized (ipmap){
                Iterator i = s.iterator();
                while(i.hasNext()){
                    Map.Entry me = (Map.Entry)i.next();
                    InetAddress ina = (InetAddress) me.getValue();
                    try {
                        Socket chk = new Socket(ina.getHostAddress(), 8000);
                        System.out.println("ping "+ me.getKey() + " " + ina.getHostAddress());

                    }
                    catch(IOException e){
                        i.remove();
                        regs.removeIP((int)me.getKey());
                    }
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
    public void addIP(int id, InetAddress i){
        synchronized (ipmap) {
            ipmap.put(id,i);
        }
    }
}
